<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">   
    <title>新規登録</title>   
    <!-- CSSの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
</head>
    
<body>
<!-- ヘッダー -->
<nav class="navbar navbar-expand-sm navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="Login">Doodle</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link disabled" href="user.html" tabindex="-1" aria-disabled="true">User</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="timeline.html" tabindex="-1" aria-disabled="true">Timeline</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="posting.html" tabindex="-1" aria-disabled="true">Post</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="topic.html" tabindex="-1" aria-disabled="true">Topic</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="search.html" tabindex="-1" aria-disabled="true">Search</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="usersList.html" tabindex="-1" aria-disabled="true">List</a>
      </li>
    </ul>
  </div>
    
</nav>

    <!-- ここから -->
<form class="form-register" action="Signup" method="post">
  <form class="px-4 py-3">
    <div class="log">
 
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

    <div class="form-group">
      <label for="id">ログインID*</label>
      <input type="text" class="form-control" name="loginId" id="id" placeholder="loginId">
    </div>
    <div class="form-group">
      <label for="password">パスワード*</label>
      <input type="password" class="form-control" name="password" id="password" placeholder="password">
    </div>
    <div class="form-group">
      <label for="password">確認用パスワード*</label>
      <input type="password" class="form-control" name="confirm" id="confirm" placeholder="confirmPassword">
    </div>
    <div class="form-group">
      <label for="text">ユーザ名*</label>
      <input type="text" class="form-control" name="name" id="name" placeholder="userName">
    </div>
    <div class="form-group">
        <label for="inputBirthday">生年月日*</label>
        <input type="date" name="birthDate" id="birthDate" class="form-control">
    </div>
    <div class="form-group">
      <label for="FormControlTextarea">ステータスメッセージ</label>
      <textarea class="form-control" id="status" rows="2" name="bio"></textarea>
    </div>

    <button class="btn btn-info" type="submit">登録</button>

  <div class="dropdown-divider"></div>
  <a href="Login" class="item">もどる</a>
    
    </div>
  </form>  
</form>

    
</body>
</html>