<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">   
    <title>検索</title>   
    <!-- CSSの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
</head>
    
<body>
<!-- ヘッダー -->
<nav class="navbar navbar-expand-sm navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="Logout">Doodle</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="User?id=${userInfo.id}">User</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Timeline">Timeline</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Posting">Post</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Topic">Topic</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="Search">Search</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UsersList">List</a>
      </li>
    </ul>
  </div>
    
</nav>
    
    <!-- ここから -->
    <!-- 検索ボックス -->
<form class="form-search" action="Search" method="post">
<div class="search">
    <div class="search-form-area">
      <div class="panel-body">
        <form class="form-horizontal">
          <div class="form-group row">
            <label for="keyword" class="col-sm-2 col-form-label">キーワード</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="keyword" id="keyword">
            </div>
          </div>
          <div class="form-group row">
            <label for="loginId" class="col-sm-2 col-form-label">ユーザID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="loginId" id="loginId">
            </div>
          </div>

          <div class="form-group row">
            <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="name" id="name">
            </div>
          </div>

          <div class="form-group row">


          <div class="text-right">
            <button type="submit" value="検索" class="btn btn-info form-submit">検索</button>
          </div>
        </form>
      </div>
    </div>
</div>
</form>
    
    <!-- 検索結果一覧 -->
<div class="list-group">
<div class="postList">
<c:forEach var="post" items="${postList}" >
  <a href="Post?id=${post.id}" class="list-group-item list-group-item-action">
    <div class="d-flex w-100 justify-content-between">
      <h6 class="mb-1">${post.udb.name}</h6>
      <small class="text-muted">${post.createDate}</small>
    </div>
    <p class="mb-1">${post.text}</p>
    <small>${post.cdb.name}</small>
  </a>
</c:forEach>
</div>
</div>
    
</body>
</html>