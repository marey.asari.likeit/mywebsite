<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">   
    <title>ユーザ情報</title>   
    <!-- CSSの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
  </head>
    
<body>
<!-- ヘッダー -->
<nav class="navbar navbar-expand-sm navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="Logout">Doodle</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="User?id=${userInfo.id}">User</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Timeline">Timeline</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Posting">Post</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Topic">Topic</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Search">Search</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="favoriteUsersList.html">FavoriteUsers
            <span class="badge badge-pill align-text-bottom">27</span>
        </a>
      </li>   
      <li class="nav-item">
        <a class="nav-link" href="FavoritePostsList">FavoritePosts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UsersList">List</a>
      </li>
    </ul>
  </div>
    
</nav>
    
    <!-- ここから --> 
<div class="user">
    <div class="alert alert-success" role="alert">メッセージ</div>
<div class="alert alert-light" role="alert">
  <h4 class="alert-heading">${user.name}</h4>
  <a>${user.loginId}</a>
  
  	<c:if test="${userInfo.loginId == user.loginId}" >
    <a class="btn btn-outline-success btn-sm" href="UserUpdate?id=${user.id}">編集</a>
    <a class="btn btn-outline-danger btn-sm" href="UserDelete?id=${user.id}">削除</a>
    </c:if>
    
    <c:if test="${userInfo.loginId == 'admin'}" >
    <a class="btn btn-outline-danger btn-sm" href="UserDelete?id=${user.id}">削除</a>
    </c:if>
    
  <div>
  <small class="text-muted">生年月日:${user.birthDate}</small>
  <small class="text-muted">登録日時:${user.createDate}</small>
    <a class="btn btn-info btn-sm" href="User?id=${user.id}">おきにいりに入れる</a>
    <a class="btn btn-secondary btn-sm" href="User?id=${user.id}">おきにいりから外す</a>
  </div>
  <hr>
  <p class="mb-0">${user.bio}</p>
</div>
</div>
    
    <!-- 投稿 -->
<div class="list-group">
<div class="usersPosts">
<c:forEach var="post" items="${postList}" >
  <a href="Post?id=${post.id}" class="list-group-item list-group-item-action">
    <div class="d-flex w-100 justify-content-between">
      <h6 class="mb-1">${post.udb.name}</h6>
      <small class="text-muted">${post.createDate}</small>
    </div>
    <p class="mb-1">${post.text}</p>
    <small>${post.cdb.name}</small>
  </a>
</c:forEach>
</div>
</div>
    
</body>
</html>