<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">   
    <title>ユーザ情報の変更</title>   
    <!-- CSSの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
</head>
    
<body>
<!-- ヘッダー -->
<nav class="navbar navbar-expand-sm navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="Logout">Doodle</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="User?id=${userInfo.id}">User</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Timeline">Timeline</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Posting">Post</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="topic.html">Topic</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Search">Search</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UsersList">List</a>
      </li>
    </ul>
  </div>
    
</nav>
    
    <!-- ここから -->
<form class="form-update" action="UserUpdate" method="post">
<input type="hidden" name="id" value="${user.id}">
  <form class="px-4 py-3">
    <div class="log">
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    <div class="form-group">
      <label for="id">ユーザID</label>
      <p class="form-control-plaintext">${user.loginId}</p>
    </div>
    <div class="form-group">
      <label for="password">パスワード</label>
      <input type="password" class="form-control" name="password" id="password" placeholder="password">
    </div>
    <div class="form-group">
      <label for="password">確認用パスワード</label>
      <input type="password" class="form-control" name="confirm" id="confirm" placeholder="confirm">
    </div>
    <div class="form-group">
      <label for="text">ユーザ名</label>
      <input type="text" class="form-control" name="name" id="name" value="${user.name}">
    </div>
    <div class="form-group">
        <label for="inputBirthday">生年月日</label>
        <p class="form-control-plaintext">${user.birthDate}</p>
    </div>
    <div class="form-group">
      <label for="FormControlTextarea">ステータスメッセージ</label>
      <textarea class="form-control" name="bio" id="bio" rows="2">${user.bio}</textarea>
    </div>

    <button class="btn btn-info" type="submit">登録</button>

  <div class="dropdown-divider"></div>
  <a class="item" href="Timeline">もどる</a>

    </div>
  </form> 
 </form>

    
</body>
</html>