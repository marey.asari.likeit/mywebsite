<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">   
    <title>コメント</title>   
    <!-- CSSの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
  </head>
    
<body>
<!-- ヘッダー -->
<nav class="navbar navbar-expand-sm navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="Logout">Doodle</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="User?id=${userInfo.id}">User</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Timeline">Timeline</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Posting">Post</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Topic">Topic</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Search">Search</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UsersList">List</a>
      </li>
    </ul>
  </div>
    
</nav>
    
    <!-- ここから -->
<div class="card" style="width: 47rem;">
  <div class="card-body">
    <h5 class="card-title">${post.udb.name}</h5>
    <a class="card-subtitle mb-2 text-muted" href="User">${post.udb.loginId}</a>
    <p class="card-text">${post.text}</p>
    <a href="Timeline" class="card-link">${post.cdb.name}</a>
    <div><small class="text-muted">${post.createDate}</small></div>
  </div>
</div>
    
<input type="hidden" name="postId" value="${id}">
<form class="form-comment" action="Comment" method="post">
<div class="comment">
    <textarea name="text" class="form-control" id="status" rows="5" placeholder="コメントを入力"></textarea>
</div>
<div class="spase">
<div class="button">
    <button class="btn btn-info" type="submit">送信</button>
</div>
</div>
</form>
    
    
</body>
</html>