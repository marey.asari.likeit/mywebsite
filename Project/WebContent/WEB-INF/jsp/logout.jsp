<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">   
    <title>ログアウト確認</title>   
    <!-- CSSの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
</head>
    
<body>
<!-- ヘッダー -->
<nav class="navbar navbar-expand-sm navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="Logout">Doodle</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="User?id=${userInfo.id}">User</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Timeline">Timeline</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Posting">Post</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Topic">Topic</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Search">Search</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UsersList">List</a>
      </li>
    </ul>
  </div>
    
</nav>
    
    <!-- ここから -->
<form class="form-signout" action="Logout" method="post">
  <form class="px-4 py-3">
      <div class="container">
          <div class="log">
     
            <p>ログアウトしますか？</p>
            <div class="row">
            <div class="col-sm-6">
                <a href="Timeline" class="btn btn-outline-secondary btn-block">いいえ</a>
            </div>
            <div class="col-sm-6">
                <button type="submit" class="btn btn-outline-info btn-block">はい</button>
            </div>
            </div>
     
        </div>
      </div>
  </form>
</form>
        
</body>
</html>