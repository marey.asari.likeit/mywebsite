<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">   
    <title>投稿詳細</title>   
    <!-- CSSの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
</head>
    
<body>
<!-- ヘッダー -->
<nav class="navbar navbar-expand-sm navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="Logout">Doodle</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="User?id=${userInfo.id}">User</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Timeline">Timeline</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Posting">Post</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Topic">Topic</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Search">Search</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="UsersList">List</a>
      </li>
    </ul>
  </div>
    
</nav>
    
    <!-- ここから -->
<div class="card" style="width: 47rem;">
     <div class="alert alert-success" role="alert">メッセージ</div>
  <div class="card-body">
    <h5 class="card-title">${post.udb.name}</h5>
    <a class="card-subtitle mb-2 text-muted" href="User">${post.udb.loginId}</a>
    <p class="card-text">${post.text}</p>
    <a href="Timeline" class="card-link">${post.cdb.name}</a>
    <a class="btn btn-info btn-sm" href="Post?id=${post.id}">おきにいりに入れる</a>
    <a class="btn btn-secondary btn-sm" href="Post?id=${post.id}">おきにいりから外す</a>
    <a class="btn btn-outline-info btn-sm" href="Comment?id=${post.id}">コメントする</a>
    <a class="btn btn-outline-danger btn-sm" href="PostDelete?id=${post.id}">削除</a>
    <div><small class="text-muted">${post.createDate}</small></div>
  </div>
</div>

<div class="list-group">
<div class="commentList">
<c:forEach var="reply" items="${replyList}" >
  <a href="Commenting" class="list-group-item list-group-item-action">
    <div class="d-flex w-100 justify-content-between">
      <h6 class="mb-1">${reply.udb.name}</h6>
      <small class="text-muted">${reply.createDate}</small>
    </div>
    <p class="mb-1">${reply.text}</p>
    <small class="text-muted">${reply.cdb.name}</small>
  </a>
</c:forEach>


</div>
</div>
    
</body>
</html>