package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBmanager;
import beans.CategoryDataBeans;
import beans.PostDataBeans;
import beans.UserDataBeans;


public class PostDAO {

	/**
	 * 投稿登録処理
	 * @param post PostDataBeans
	 * @throws SQLException
	 * 			呼び出し元にスローさせるため
	 */
	public static void insertPost(PostDataBeans post) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBmanager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO post(user_id,text,category_id,create_date) VALUES(?,?,?,NOW())");
			st.setInt(1, post.getUserId());
			st.setString(2, post.getText());
			st.setInt(3, post.getCategoryId());
			st.executeUpdate();
			System.out.println("inserting Post has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}




	public void deletePost(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// INSERT文を準備
			String sql = "DELETE FROM post WHERE id=?";
			// 実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			pStmt.executeUpdate();
			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 全ての投稿情報を取得する
	 * @return
	 */
	public List<PostDataBeans> findAllPosts() {
		Connection conn = null;
		List<PostDataBeans> postList = new ArrayList<PostDataBeans>();

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT "
							+ "post.id as postId, "
							+ "post.text, "
							+ "post.create_date, "
							+ "category.name as categoryName, "
							+ "user.name as userName "
						+ "FROM "
							+ "post "
						+ "INNER JOIN "
							+ "category "
						+ "ON "
							+ "post.category_id = category.id "
						+ "INNER JOIN "
							+ "user "
						+ "ON "
							+ "post.user_id = user.id ";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {

				// 必要なデータのみインスタンスのフィールドに追加
				int idData = rs.getInt("postId");
				String textData = rs.getString("text");
				Date createDateData = rs.getDate("create_date");

				String categoryNameData = rs.getString("categoryName");

				String userNameData = rs.getString("userName");

			    UserDataBeans udb = new UserDataBeans();
			    udb.setName(userNameData);

			    CategoryDataBeans cdb = new CategoryDataBeans();
			    cdb.setName(categoryNameData);


				PostDataBeans post = new PostDataBeans(idData, textData, createDateData, cdb, udb);

				postList.add(post);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return postList;
	}

	public List<PostDataBeans> findById(String id) {
		Connection conn = null;
		List<PostDataBeans> postList = new ArrayList<PostDataBeans>();

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT "
							+ "post.id as postId, "
							+ "post.text, "
							+ "post.create_date, "
							+ "category.name as categoryName, "
							+ "user.name as userName "
						+ "FROM "
							+ "post "
						+ "INNER JOIN "
							+ "category "
						+ "ON "
							+ "post.category_id = category.id "
						+ "INNER JOIN "
							+ "user "
						+ "ON "
							+ "post.user_id = user.id "
						+ "WHERE "
							+ "user.id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {

				// 必要なデータのみインスタンスのフィールドに追加
				int idData = rs.getInt("postId");
				String textData = rs.getString("text");
				Date createDateData = rs.getDate("create_date");

				String categoryNameData = rs.getString("categoryName");

				String userNameData = rs.getString("userName");

			    UserDataBeans udb = new UserDataBeans();
			    udb.setName(userNameData);

			    CategoryDataBeans cdb = new CategoryDataBeans();
			    cdb.setName(categoryNameData);


				PostDataBeans post = new PostDataBeans(idData, textData, createDateData, cdb, udb);

				postList.add(post);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return postList;
	}


	public List<PostDataBeans> findSearch(String keywordP, String loginIdP, String nameP) {
		Connection conn = null;
		List<PostDataBeans> postList = new ArrayList<PostDataBeans>();

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT "
							+ "post.id as postId, "
							+ "post.text, "
							+ "post.create_date, "
							+ "category.name as categoryName, "
							+ "user.login_id, "
							+ "user.name as userName "
						+ "FROM "
							+ "post "
						+ "INNER JOIN "
							+ "category "
						+ "ON "
							+ "post.category_id = category.id "
						+ "INNER JOIN "
							+ "user "
						+ "ON "
							+ "post.user_id = user.id "

						+ "WHERE "
						+ "1 = 1 ";

			if(!keywordP.equals("")) {
				sql += " AND post.text LIKE '%" + keywordP + "%'";
			}

			if(!loginIdP.equals("")) {
				sql += " AND user.login_id = '" + loginIdP + "'";
			}

			if(!nameP.equals("")) {
				sql += " AND user.name LIKE '%" + nameP + "%'";
			}

			// 以下の方法でも可能だが、項目数が多くなると条件追加が大変
//			if(!keywordP.equals("")) {
//				sql += " post.text LIKE '%" + keywordP + "%'";
//			}
//
//			if(!loginIdP.equals("") && !keywordP.equals("")) {
//				sql += " AND user.login_id = '" + loginIdP + "'";
//			}else if(!loginIdP.equals("")){
//				sql += " user.login_id = '" + loginIdP + "'";
//			}
//
//			if(!nameP.equals("")&&(!loginIdP.equals("") || !keywordP.equals(""))) {
//				sql += " AND userName LIKE '%" + nameP + "%'";
//			}else if(!nameP.equals("")){
//				sql += " userName LIKE '%" + nameP + "%'";
//			}

			System.out.println(sql);

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int idData = rs.getInt("postId");

				String textData = rs.getString("text");
				Date createDateData = rs.getDate("create_date");

				String categoryNameData = rs.getString("categoryName");

				String loginIdData = rs.getString("login_id");
				String userNameData = rs.getString("userName");

			    UserDataBeans udb = new UserDataBeans();
			    udb.setLoginId(loginIdData);
			    udb.setName(userNameData);

			    CategoryDataBeans cdb = new CategoryDataBeans();
			    cdb.setName(categoryNameData);
				PostDataBeans post = new PostDataBeans(idData, textData, createDateData, cdb, udb);

				postList.add(post);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return postList;
	}


	/**
	 * カテゴリIDによる投稿検索
	 * @param categoryId
	 * @return {PostDataBeans}
	 * @throws SQLException
	 */



	public List<PostDataBeans> findByCategoryId(int categoryId) {
		Connection conn = null;
		List<PostDataBeans> postList = new ArrayList<PostDataBeans>();
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();


			// SELECT文を準備
			String sql =  "SELECT "
							+ "post.id as postId, "
							+ "post.text, "
							+ "post.create_date, "
							+ "category.name as categoryName, "
							+ "user.name as userName "
						+ "FROM "
							+ "post "
						+ "INNER JOIN "
							+ "category "
						+ "ON "
							+ "post.category_id = category.id "
						+ "INNER JOIN "
							+ "user "
						+ "ON "
							+ "post.user_id = user.id "
						+ "WHERE "
							+ "category.id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, categoryId);
			ResultSet rs = pStmt.executeQuery();


			while (rs.next()) {

				// 必要なデータのみインスタンスのフィールドに追加
				int idData = rs.getInt("postId");
				String textData = rs.getString("text");
				Date createDateData = rs.getDate("create_date");

				String categoryNameData = rs.getString("categoryName");

				String userNameData = rs.getString("userName");

			    UserDataBeans udb = new UserDataBeans();
			    udb.setName(userNameData);

			    CategoryDataBeans cdb = new CategoryDataBeans();
			    cdb.setName(categoryNameData);


				PostDataBeans post = new PostDataBeans(idData, textData, createDateData, cdb, udb);

				postList.add(post);
			}



		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return postList;
	}

	/**
	 * IDから投稿情報を取得する
	 *
	 */
	public static PostDataBeans browseByPost(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT "
							+ "post.id, "
							+ "post.text, "
							+ "post.create_date, "
							+ "category.name as categoryName, "
							+ "user.login_id, "
							+ "user.name as userName "
						+ "FROM "
							+ "post "
						+ "INNER JOIN "
							+ "category "
						+ "ON "
							+ "post.category_id = category.id "
						+ "INNER JOIN "
							+ "user "
						+ "ON "
							+ "post.user_id = user.id "
						+ "WHERE "
							+ "post.id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int idData = rs.getInt("id");
			String textData = rs.getString("text");
			Date createDateData = rs.getDate("create_date");

			String categoryNameData = rs.getString("categoryName");

			String loginIdData = rs.getString("login_id");
			String userNameData = rs.getString("userName");

		    UserDataBeans udb = new UserDataBeans();
		    udb.setLoginId(loginIdData);
		    udb.setName(userNameData);

		    CategoryDataBeans cdb = new CategoryDataBeans();
		    cdb.setName(categoryNameData);

			return new PostDataBeans(idData, textData, createDateData, cdb, udb);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


}
