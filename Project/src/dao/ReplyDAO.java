package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBmanager;
import beans.ReplyDataBeans;

public class ReplyDAO {

	/**
	 * 投稿登録処理
	 * @param bddb PostDataBeans
	 * @throws SQLException
	 * 			呼び出し元にスローさせるため
	 */
	public static void insertReply(ReplyDataBeans reply) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBmanager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO post(user_id,text,post_id,create_date) VALUES(?,?,?,NOW())");
			st.setInt(1, reply.getUserId());
			st.setString(2, reply.getText());
			st.setString(2, reply.getPostId());
			st.executeUpdate();
			System.out.println("inserting Reply has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public void deleteReply(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// INSERT文を準備
			String sql = "DELETE FROM reply WHERE id=?";
			// 実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			pStmt.executeUpdate();
			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
