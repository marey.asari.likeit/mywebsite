package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBmanager;
import beans.FavoriteUserDataBeans;

public class FavoriteUserDAO {

	/**
	 * 投稿登録処理
	 * @param bddb PostDataBeans
	 * @throws SQLException
	 * 			呼び出し元にスローさせるため
	 */
	public static void insertFavoritePost(FavoriteUserDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBmanager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO f_post(user_id,f_user_id) VALUES(?,?)");
			st.setInt(1, bddb.getUserId());
			st.setInt(2, bddb.getFavoritedUserId());
			st.executeUpdate();
			System.out.println("inserting FavoriteUser has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public void deleteFavoriteUser(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// INSERT文を準備
			String sql = "DELETE FROM f_user WHERE id=?";
			// 実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			pStmt.executeUpdate();
			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 全てのおきにいりユーザ情報を取得する
	 * @return
	 */
	public List<FavoriteUserDataBeans> findFavoriteUsers() {
		Connection conn = null;
		List<FavoriteUserDataBeans> fUserList = new ArrayList<FavoriteUserDataBeans>();

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM f_uost WHERE user_id=?";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int favoritedUserId = rs.getInt("f_user_id");
				FavoriteUserDataBeans fUser = new FavoriteUserDataBeans(id, userId, favoritedUserId);

				fUserList.add(fUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return fUserList;
	}

}
