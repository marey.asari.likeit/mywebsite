package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBmanager;
import beans.CategoryDataBeans;


public class CategoryDAO {

	/**
	 * DBに登録されているカテゴリを取得
	 * @return {DeliveryMethodDataBeans}
	 * @throws SQLException
	 */
	public static ArrayList<CategoryDataBeans> getAllCategoryDataBeans() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBmanager.getConnection();

			st = con.prepareStatement("SELECT * FROM category");

			ResultSet rs = st.executeQuery();

			ArrayList<CategoryDataBeans> categoryDataBeansList = new ArrayList<CategoryDataBeans>();
			while (rs.next()) {
				CategoryDataBeans category = new CategoryDataBeans();
				category.setId(rs.getInt("id"));
				category.setName(rs.getString("name"));
				categoryDataBeansList.add(category);
			}

			System.out.println("searching all CategoryDataBeans has been completed");

			return categoryDataBeansList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * カテゴリをIDをもとに取得
	 * @param DeliveryMethodId
	 * @return DeliveryMethodDataBeans
	 * @throws SQLException
	 */
	public static CategoryDataBeans getCategoryDataBeansByID(int CategoryId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBmanager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM category WHERE id = ?");
			st.setInt(1, CategoryId);

			ResultSet rs = st.executeQuery();

			CategoryDataBeans dmdb = new CategoryDataBeans();
			if (rs.next()) {
				dmdb.setId(rs.getInt("id"));
				dmdb.setName(rs.getString("name"));
			}

			System.out.println("searching CategoryDataBeans by CategoryID has been completed");

			return dmdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
     *投稿IDによるカテゴリ検索
     * @param buyId
     * @return dmdb CategoryDataBeans
     *             カテゴリ情報に対応するデータを持つJavaBeans
     * @throws SQLException
     */
	public static CategoryDataBeans getCategoryDataBeansByPostId(int PostId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBmanager.getConnection();

			st = con.prepareStatement(
					"SELECT category.name"
					+ " FROM post"
					+ " JOIN category"
					+ " ON category.id = post.category_id"
					+ " WHERE post.id = ?");
			st.setInt(1, PostId);

			ResultSet rs = st.executeQuery();
			CategoryDataBeans dmdb = new CategoryDataBeans();

			while (rs.next()) {
				dmdb.setName(rs.getString("name"));

			}

			System.out.println("searching CategoryDataBeans by PostID has been completed");
			return dmdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
