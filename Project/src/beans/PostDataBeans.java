package beans;

import java.io.Serializable;
import java.util.Date;

public class PostDataBeans implements Serializable {
	private int id;
	private int userId;
	private String text;
	private int categoryId;
	private Date createDate;
	private CategoryDataBeans cdb;
	private UserDataBeans udb;

	// ぜんぶのコンストラクタ
	public PostDataBeans(int id, int userId, String text, int categoryId, Date createDate)  {
		this.id = id;
		this.userId = userId;
		this.text = text;
		this.categoryId = categoryId;
		this.createDate = createDate;
	}

	public PostDataBeans(int id, int userId, String text, Date createDate)  {
		this.id = id;
		this.userId = userId;
		this.text = text;
		this.createDate = createDate;
	}

	public PostDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public PostDataBeans(int idData, String textData, Date createDateData, CategoryDataBeans cdb, UserDataBeans udb) {
		this.id = idData;
		this.text = textData;
		this.createDate = createDateData;
		this.cdb = cdb;
		this.udb = udb;
	}

	public PostDataBeans(String textData, Date createDateData, CategoryDataBeans cdb, UserDataBeans udb) {
		this.text = textData;
		this.createDate = createDateData;
		this.cdb = cdb;
		this.udb = udb;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public CategoryDataBeans getCdb() {
		return cdb;
	}

	public void setCdb(CategoryDataBeans cdb) {
		this.cdb = cdb;
	}

	public UserDataBeans getUdb() {
		return udb;
	}

	public void setUdb(UserDataBeans udb) {
		this.udb = udb;
	}


}
