package beans;

import java.io.Serializable;

public class UserDataBeans implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private String birthDate;
	private String password;
	private String confirm;
	private String bio;
	private String createDate;
	private String updateDate;
	
	public UserDataBeans() {

	}
	
	// ログインセッションを保存するためのコンストラクタ
	public UserDataBeans(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	// ぜんぶのコンストラクタ
	public UserDataBeans(int id, String loginId, String name, String birthDate, String password, String bio,
			String createDate, String updateDate)  {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.bio = bio;
	}



	public UserDataBeans(int userId, String loginIdData, String nameData) {
		this.id = userId;
		this.loginId = loginIdData;
		this.name = nameData;
	}

	public void updateUser(String name, String password, String bio) {
		this.name = name;
		this.password = password;
		this.bio = bio;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


}
