package beans;

import java.io.Serializable;

public class FavoriteUserDataBeans implements Serializable {
	private int id;
	private int userId;
	private int favoritedUserId;

	// ぜんぶのコンストラクタ
	public FavoriteUserDataBeans(int id, int userId, int favoritedUserId)  {
		this.id = id;
		this.userId = userId;
		this.favoritedUserId = favoritedUserId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getFavoritedUserId() {
		return favoritedUserId;
	}

	public void setFavoritedUserId(int favoritedUserId) {
		this.favoritedUserId = favoritedUserId;
	}


}
