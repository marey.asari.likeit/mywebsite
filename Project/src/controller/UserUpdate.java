package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;


/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログインセッションの有無の判定
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			// ログイン画面にリダイレクト
			response.sendRedirect("Login");
			return;
		}
		
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);


		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する

		UserDataBeans user = UserDAO.browseByUser(id);
		
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("user", user);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String password = request.getParameter("password");
		String confirm = request.getParameter("confirm");
		String name = request.getParameter("name");
		String bio = request.getParameter("bio");
		String id = request.getParameter("id");
		
		
		
		
		/** 登録失敗 **/
		if (!password.equals(confirm)) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードと確認用パスワードが不一致です");
			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
			
		} else if(name.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ユーザ名が未入力です");
			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
			
		}

		
		if(password.equals("") && confirm.equals("")) {
			UserDAO.updateUser(name, bio,id);
		}else {
			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			UserDAO.updateUser(name, password, bio,id);
		}
		


		// 投稿一覧のサーブレットにリダイレクト
		response.sendRedirect("Timeline");
	}

}
