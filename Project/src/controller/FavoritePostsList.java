package controller;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.FavoritePostDataBeans;
import dao.FavoritePostDAO;

/**
 * Servlet implementation class FavoritePostsList
 */
@WebServlet("/FavoritePostsList")
public class FavoritePostsList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FavoritePostsList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/** ログインセッションがない場合、ログイン画面にリダイレクトさせる **/
		// ログインセッションの有無の判定
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			// ユーザ一覧画面にリダイレクト
			response.sendRedirect("Login");
			return;
		}
		
		// ユーザ一覧情報を取得
		FavoritePostDAO favoritePostDAO = new FavoritePostDAO();
		List<FavoritePostDataBeans> fPostList = favoritePostDAO.findFavoritePosts();
		Collections.reverse(fPostList);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("fPostList", fPostList);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/favoritePostsList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
