package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryDataBeans;
import beans.PostDataBeans;
import dao.CategoryDAO;
import dao.PostDAO;

/**
 * Servlet implementation class Timeline
 */
@WebServlet("/Timeline")
public class Timeline extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Timeline() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/** ログインセッションがない場合、ログイン画面にリダイレクトさせる **/
		// ログインセッションの有無の判定
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			// ユーザ一覧画面にリダイレクト
			response.sendRedirect("Login");
			return;
		}
		
		// カテゴリ情報を取得
		ArrayList<CategoryDataBeans> categoryList = new ArrayList<CategoryDataBeans>();
		try {
			categoryList = CategoryDAO.getAllCategoryDataBeans();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		request.setAttribute("categoryList", categoryList);
		
		// 投稿一覧情報を取得
		PostDAO postDAO = new PostDAO();
		List<PostDataBeans> postList = postDAO.findAllPosts();
		Collections.reverse(postList);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("postList", postList);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/timeline.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		//選択されたカテゴリIDを取得
		int categoryId = Integer.parseInt(request.getParameter("categoryId"));

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		PostDAO postDAO = new PostDAO();
		List<PostDataBeans> post = postDAO.findByCategoryId(categoryId);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("postList", post);

		// 投稿一覧のサーブレットにリダイレクト
		response.sendRedirect("Timeline");

	}

}
