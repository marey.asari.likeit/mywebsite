package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PostDataBeans;
import beans.ReplyDataBeans;
import beans.UserDataBeans;
import dao.PostDAO;
import dao.ReplyDAO;

/**
 * Servlet implementation class comment
 */
@WebServlet("/comment")
public class comment extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public comment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログインセッションの有無の判定
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			// ログイン画面にリダイレクト
			response.sendRedirect("Login");
			return;
		}

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);


		// TODO  未実装：idを引数にして、idに紐づくユーザー情報を出力する
		PostDataBeans post = PostDAO.browseByPost(id);
		request.setAttribute("post", post);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/comment.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String text = request.getParameter("text");
		String postId = request.getParameter("post_id");


		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");
		ReplyDataBeans reply = new ReplyDataBeans();
		reply.setUserId(user.getId());
		reply.setText(text);
		reply.setPostId(postId);


		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		try {
			ReplyDAO.insertReply(reply);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


		session.setAttribute("reply", reply);

		// 投稿一覧のサーブレットにリダイレクト
		response.sendRedirect("Timeline");
	}

}
