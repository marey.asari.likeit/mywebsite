package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryDataBeans;
import beans.PostDataBeans;
import beans.UserDataBeans;
import dao.CategoryDAO;
import dao.PostDAO;


/**
 * Servlet implementation class Posting
 */
@WebServlet("/Posting")
public class Posting extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Posting() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログインセッションの有無の判定
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			// ログイン画面にリダイレクト
			response.sendRedirect("Login");
			return;
		}
		
		// カテゴリ情報を取得
		ArrayList<CategoryDataBeans> categoryList = new ArrayList<CategoryDataBeans>();
		try {
			categoryList = CategoryDAO.getAllCategoryDataBeans();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		request.setAttribute("categoryList", categoryList);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/posting.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String text = request.getParameter("text");


		//選択されたカテゴリIDを取得
		int inputCategoryId = Integer.parseInt(request.getParameter("categoryId"));


		
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");
		PostDataBeans post = new PostDataBeans();
		post.setUserId(user.getId());
		post.setCategoryId(inputCategoryId);
		post.setText(text);


		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		try {
			PostDAO.insertPost(post);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


		session.setAttribute("post", post);

		// 投稿一覧のサーブレットにリダイレクト
		response.sendRedirect("Timeline");
	}

}
