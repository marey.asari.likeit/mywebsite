CREATE DATABASE doodle DEFAULT CHARACTER SET utf8;
USE doodle;

CREATE TABLE user( 
  id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT
  , login_id varchar (255) UNIQUE NOT NULL
  , name varchar (255) NOT NULL
  , birth_date DATE NOT NULL
  , password varchar (255) NOT NULL
  , bio varchar (255)
  , create_date DATETIME NOT NULL
  , update_date DATETIME NOT NULL
);

CREATE TABLE category( 
  id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT
  , name varchar (255) NOT NULL
);

CREATE TABLE post( 
  id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT
  , user_id varchar (255) NOT NULL
  , text varchar (255) NOT NULL
  , category_id int (11) NOT NULL
  , create_date DATETIME NOT NULL
);

CREATE TABLE reply( 
  id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT
  , user_id varchar (255) NOT NULL
  , text varchar (255) NOT NULL
  , post_id int (11) NOT NULL
  , create_date DATETIME NOT NULL
);

CREATE TABLE f_user( 
  id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT
  , user_id varchar (255) NOT NULL
  , f_user_id int (11) NOT NULL
);

CREATE TABLE f_post( 
  id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT
  , user_id varchar (255) NOT NULL
  , post_id int (11) NOT NULL
);

CREATE TABLE f_reply( 
  id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT
  , user_id varchar (255) NOT NULL
  , reply_id int (11) NOT NULL
);


insert into category value ('1','生活');
insert into category value ('2','趣味');
insert into category value ('3','質問');
insert into category value ('4','その他');


insert into post values ('1','1','tlのｋｓくにん','4','2020-04-02 00:00:00');
insert into post values ('2','1','リストの確認','4','2020-04-02 00:00:00');
insert into post values ('3','2','これはちがうひと','4','2020-04-02 00:00:00');
insert into post values ('4','2','カテゴリtojikan','3','2020-04-02 01:00:00');


